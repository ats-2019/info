clear()

function res = annule_diago(mat)
	// Fenvoie la matrice carrée passée en argument en mettant tous les éléments
	// de la diagonale à 0.
	// Exemples:
	// annule_diago([1 1; 1 1]) == [0 1; 1 0]
	// annule_diago([1 2 3; 4 5 6; 7 8 9]) == [0 2 3; 4 0 6; 7 8 0])

    // Des tests sont disponibles en bas de fichier.
    // ...
endfunction

function res = somme(mat1, mat2)
	// Renvoie la somme de mat1 et mat2. Lance une erreur si les matrices
	// ne sont pas de mêmes dimensions.
	// Exemples:
	// somme([1 2], [5 -1]) == [6 1]
	// somme([3 4; -1 2], [1 -3; 2 1]) == [4 1; 1 3]


	// Bien sûr, on s'interdit d'utiliser l'opérateur "+" entre les matrices de
	// Scilab. Des tests sont disponibles en bas de fichier.
	if ~isequal(size(mat1), size(mat2)) then
		error("Les matrices n''ont pas les mêmes dimensions")
	end
    [n_lignes, n_cols] = size(mat1)
	res = zeros(n_lignes, n_cols)
	// ...
endfunction


function res = mult(mat1, mat2)
	// Renvoie le produit de mat1 et mat2 (dans cet ordre). Lance une erreur si
	// les matrices n'ont pas des dimensions compatibles.
	// Exemples :
	// mult([1 2; 2 -1], [1; 3]) == [7; -1]
	// mult([1 2 -1; 3 -1 1], [1 2; 2 -1; 3 -1]) == [2 1; 4 6]
	
	// Écrire d'abord la fonction de tests en fin de fichier avant de réaliser
	// cette fonction.

	// Bien sûr, on s'interdit d'utiliser l'opérateur "+" entre les matrices de
	// Scilab.

	// ...

endfunction

function res = identite(n)
	// Renvoie la matrice identité d'ordre n.

	// Écrire d'abord la fonction de tests avant de programmer cette fonction.

	// Interdiction d'utiliser la fonction "eye", bien sûr ! On peut utiliser
	// les fonctions "ones" ou "zeros".

	// ...
endfunction

////////////////////////////////////////////////////////////////////////////////
//							TESTS 											  //
////////////////////////////////////////////////////////////////////////////////

// Les fonctions suivantes réalisent des tests. Elles ne sont pas appelées, donc
// pour l'instant aucun test ne s'effectue vraiment. C'est à vous d'appeler ces
// fonctions pour effectuer les tests.

function tests_annule()
	ok = %t
	if ~isequal(annule_diago([1 1; 1 1]), [0 1; 1 0]) then
		ok = %f
		disp("annule_diago([1 1; 1 1]) renvoie:")
		disp(annule_diago([1 1; 1 1]))
		disp('au lieu de:');
		disp([0 1; 1 0])
	end

	if ~isequal(annule_diago([1 2 3; 4 5 6; 7 8 9]), [0 2 3; 4 0 6; 7 8 0]) then
		ok = %f
		disp("annule_diago([1 2 3; 4 5 6; 7 8 9]) renvoie:")
		disp(annule_diago([1 2 3; 4 5 6; 7 8 9]))
		disp('au lieu de:');
		disp([0 2 3; 4 0 6; 7 8 0])
	end

	if ok then
		disp("Tous les tests passés avec succès pour annule_diago")
	end
endfunction

function tests_somme()
	ok = %t
	if ~isequal(somme([1 2], [5 -1]), [6 1]) then
		ok = %f
		disp("somme([1 2], [5 -1]) renvoie:")
		disp(somme([1 2], [5 -1]))
		disp('au lieu de:');
		disp([6 1])
	end

	if ~isequal(somme([3 4; -1 2], [1 -3; 2 1]), [4 1; 1 3]) then
		ok = %f
		disp("somme([3 4; -1 2], [1 -3; 2 1]) renvoie:")
		disp(somme([3 4; -1 2], [1 -3; 2 1]))
		disp('au lieu de:');
		disp([4 1; 1 3])
	end

	if ok then
		disp("Tous les tests passés avec succès pour somme")
	end
endfunction

function tests_mult()
	// ...
endfunction

function tests_idenite()
	// ...
endfunction
