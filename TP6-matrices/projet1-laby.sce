clear()
clf()
// Ce projet s'intéresse à la résolution de labyrinthes. On ne considère ici que
// les labyrinthes dont la sortie et l'entrée sont  "sur les bords" du
// labyrinthe. Ainsi la méthode "de la main gauche" garanti de trouver à tous
// les coups un chemin vers la sortie (ça ne serait pas forcément le cas si la
// sortie ou l'entrée se trouvait au milieu, par exemple si on vous dépose en
// hélicoptère au coeur du labyrinthe).

// La méthode de la "main gauche" consiste à longer le mur à notre gauche (en
// faisant glisser sa main gauche sur le mur par exemple).

function point = creer_point_mobile(marqueur, taille, fond)
	// Crée un point sur un graphique qu'on peut "déplacer" après sa création.
	// Par défaut, le point est placé à l'origine du repère (0, 0).
	plot(0, 0, marqueur, "marksize", taille, "markbackground", fond)
	point = gce()
endfunction

function deplacer(point, pos)
	// Déplace un point créé par la fonction creer_point_mobile aux coordonnes
	// "pos".
	point.children.data = pos
endfunction

function changer_marqueur(point, marqueur)
	// Change le style du marqueur ("marqueur" est alors un entier)
	point.children.mark_style = marqueur
endfunction

// Constantes pour les marqueurs.
FLECHE_HAUT = 6
FLECHE_BAS = 7
FLECHE_GAUCHE = 13
FLECHE_DROITE = 12

TEMPS_REPOS = 500


// On définit "vide" ou "mur" grâce à des constantes; on pourra alors
// facilement changer les couleurs en modifiant les valeurs de ces constantes.
VIDE = 8
MUR = 1

function laby_res =convert_laby(laby_bool)
	// La matrice "laby_bool" est consistuée de 0 (codant les murs)
	// et de 1 (codant les vides, où on peut marcher).
	// La fonction renvoie "laby_res" qui a les même dimensions que "laby_bool"
	// les 0 étant remplacés par la constante MUR et les 1 étant remplacés par
	// la constante VIDE.

endfunction

function deplacer_fleche(laby, fleche, pos, vit)
	// Place la flèche à la bonne position et change son marqueur en fonction
	// de la vitesse. 
	// ATTENTION : la case (i, j) de la matrice ne s'affiche pas du tout 
	// au point de coordonnées (i, j) !!

endfunction

function resoud_laby(laby)
	// Affiche les différentes étapes de la résolution du labyrinthe "laby"
	// passé en paramètre. Le départ est la case de coordonnées (2,2), l'arrivée
	// est en (n_lignes-1, n_colonnes-1). Le tour du labyrinthe est toujours un
	// mur.

	// L'algorithme est le suivant: la position est reperé par "pos" et le
	// vecteur vitesse par "vit". On regarde successivement (et dans cet ordre
	// les cases) :
	// * à gauche de la position courante (dans le "sens de la marche")
	// * en face de la position courante,
	// * à droite
	// * derrière nous.
	// On se déplace alors vers la première case "VIDE", puis on recommence.
	// Indication: on pourra utiliser des matrices de rotation pour "tourner à
	// gauche" ou "tourner à droite".
	pos = [2; 2]
	vit = [1; 0]

endfunction

L1 = [
	0 0 0 0 0 0 0
	0 1 1 1 1 1 0
    0 1 0 0 0 0 0
    0 1 0 1 0 0 0
    0 1 1 1 1 1 0
    0 0 0 0 0 0 0
]

L2 = [
	0 0 0 0 0 0 0 0 0 0 
    0 1 1 1 1 1 0 0 1 0 
    0 0 1 0 1 0 0 1 1 0 
    0 1 1 0 1 1 0 1 0 0 
    0 1 0 1 1 0 0 1 1 0 
    0 1 0 0 1 0 0 0 1 0 
    0 1 0 0 1 1 1 1 1 0 
    0 1 1 0 1 0 0 0 0 0 
    0 1 0 0 1 1 1 1 1 0 
    0 0 0 0 0 0 0 0 0 0
]
laby1 = convert_laby(L2)
resoud_laby(laby1)

