clf()
clear()
a = get("current_axes")
a.isoview = "on"

// Ce projet implémente un "jeu de la vie".
// Toutes les cellules d'une grille sont initialisée à VIVANT ou MORT.
// À chaque étape, chaque cellule évolue en fonction du contenu de ses 8
// voisines :
//      Si 2 voisins sont vivants, la cellule ne change pas d'état
//      Si 3 voisins sont vivants, la cellule devient vivante
//      Dans tous les autres cas, la cellule meurt

// On définit "vivant" ou "mort" grâce à des constantes; on pourra alors
// facilementchanger les couleurs en modifiant les valeurs de ces constantes.
VIVANT = 5
MORT = 1

function mat_vie = conversion(mat_bool)
	// La matrice "mat_bool" est consistuée de 0 (codant les cellules mortes)
	// et de 1 (codant les cellules vivantes).
	// La fonction renvoie "mat_vie" qui a les même dimensions que "mat_bool"
	// les 0 étant remplacés par la constante MORT et les 1 étant remplacés par
	// la constante VIVANT.

endfunction

function nb_vivants = nb_vivants_autour(mat, i, j)
    // Renvoie le nombre de cellules vivantes autour de la case (i, j) de la
    // matrice mat. On considère la grille "torique", c'est à dire que le bas et
    // le haut communiquent ainsi que la gauche et la droite (on peut s'imaginer
    // que la grille est dessinée sur un donut).

    //On pourra utiliser la fonction "pmodulo(a, m)" qui calcule le reste de la
    //division euclidienne (entière) de a par m. Astuce du russe: "-1) +1"

endfunction
    


function mat_res = etape(mat_init)
    // Effectue une étape du jeu de la vie à partir de la matrice mat_init en
    // construisant une nouvelle matrice dans mat_res.

endfunction

function jeu(mat)
    // Affiche successivement les différentes étapes de la grille, la grille de
    // départ étant l'argument "mat". Ne s'arrête que sur un "ctrl+c abort".

    // (pensez à mettre un "sleep" entre chaque affichage)

endfunction

// Les deux grilles suivantes sont "cycliques". La première l'est réellement, la
// seconde "avance" d'un cycle à l'autre.
cyclique_en_place = [
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 1 1 0 1 1 1 1 1 1 0 0 0 
 0 0 0 1 1 0 1 1 1 1 1 1 0 0 0
 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0
 0 0 0 1 1 0 0 0 0 0 1 1 0 0 0
 0 0 0 1 1 0 0 0 0 0 1 1 0 0 0
 0 0 0 1 1 0 0 0 0 0 1 1 0 0 0
 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0
 0 0 0 1 1 1 1 1 1 0 1 1 0 0 0
 0 0 0 1 1 1 1 1 1 0 1 1 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
]

cyclique_avance = [
 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 1 0 0 1 0 0 0 0 0 0
 0 0 0 0 0 0 1 0 0 0 0 0
 0 0 1 0 0 0 1 0 0 0 0 0
 0 0 0 1 1 1 1 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0
]

// Créer au moins 2 grilles stables, c'est à dire qui ne changent pas lorsqu'on
// effectue une étape (autre que "que des 0") :
stable1 = [
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
]

stable2 = [
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0
]



grille = conversion(cyclique_en_place)
jeu(grille)
