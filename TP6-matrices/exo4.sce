// Cet exercice n'en est pas vraiment un, il s'agit essentiellement de montrer
// l'utilisation de la commande Matplot.

// Plutot qu'un long discours, un exemple :
clf()
Matplot([1 2 3 4 5; 6 7 8 9 10])

// Jouez un peu avec la commande précédente pour comprendre comment elle
// fonctionne. Notamment, comprenez à quelles coordonnées s'affichent la case 
// (1,1), (1,2) et plus généralement (i, j).

// Tapez "help Matplot" dans la console pour voir les couleurs disponibles.
