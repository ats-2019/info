// Scilab gère nativement les matrices (des tableaux de nombres avec des lignes
// et des colonnes) ; c'est d'ailleurs sa principale raison d'être. Nous avons
// déjà manipulé ces objets, en nous limitant aux tableaux à 1 lignes et
// plusieurs colonnes. Voici plusieurs syntaxes donnant la même matrice :

// DEFINIION D'UNE MATRICE
//-------------------------

m = [1 2 3; 4 5 6] // les lignes sont séparés par un ";" et les colonnes par ","
m = [1, 2, 3; 4, 5, 6] // les colonnes séparés par ","
m = [ // les lignes sont séparés par un retour à la ligne
    1 2 3
    4 5 6
]

// On ne peut pas définir une matrice où le nombre de colonnes varie d'une ligne
// à l'autre. Par exemple la ligne suivante provoque une erreur :
t = [
    1 2 3
    4 5
]

// On peut également construire des matrices grâces aux fonctions "zeros" et
// "ones":
zeros(5, 7) // construit une matrice de 5 lignes et 7 colonnes remplie de 0
ones(3, 9) // construit une matrice de 3 lignes et 9 colonnes remplie de 1



// On peut définir la matrice idententié grâce à la fonction "eye":
eye(3, 3) // crée la matrice [1 0 0; 0 1 0; 0 0 1]


// ACCES AUX ÉLÉMENTS
// ------------------

// On peut accéder aux éléments grâce à la syntaxe "m(i, j)"; le "i" désigne le
// numéro de la ligne et le "j" désigne le numéro de colonne. Par exemple (avec
// la matrice m définie ci-dessus):
m(2, 1) // correspond à 4
m(2, 3) // correspond à 6
m(1, 1) // correspond à 1

// On peut modifier les éléments grâce à la même syntaxe :
m(2, 1) = 0 // Après cette ligne la matrice devient [1 2 3; 0 5 6]

// TAILLE DE LA MATRICE
// --------------------

// On peut encore appeler la fonction "length" sur une matrice, cela donne le
// nombre d'éléments de la matrice. La fonction "size" en revanche renvoie une
// matrice à une ligne et deux colonnes contenant le nombre de lignes puis le
// nombre de colonnes. Exemple (toujours avec la même matrice).
length(m) // donne 6
size(m) // donne [2 3]

// On peut récupérer le nombre de lignes et colonnes ainsi :
[n_ligs, n_cols] = size(m)


// OPÉRATIONS SUR LES MATRICES
// ---------------------------

// On peut encore additionner, soustraire des matrices entre elles, comme
// on le fait en maths:
m1 = [1 2 3; 4 5 6]
m2 = [7 8 9; 10 11 12]
m1 + m2 // donne : [8 10 12; 14 16 18]
m2 - m1 // donne : [6 6 6; 6 6 6]



// On peut effectuer des multiplications, division, puissance terme à terme avec
// les opérateurs ".*", "./", ".^"  (noter le point avant l'opérateur).
m1 .* m2 // donne [7 16 27; 40 55 72]
[2 3 5] .^ [2 3 1] // donne [4 27 5]

// On peut calculer la transposée (les lignes deviennent les colonnes) avec
// l'apostrophe '.
m1' // donne [1 4; 2 5; 3 6]

// Et enfin, on peut calculer le produit matriciel avec "*". On ne peut le faire
// qu'entre des matrices de tailles compatibles, c'est à dire que la première
// matrice doit avoir autant de colonnes que la seconde a de lignes.

// Par exemple, on ne peut pas faire le produi de m1 et m2 (m1 a 3 colonnes et
// m2 a 2 lignes).

[1 2; 3 4] * [1; 2] // donne [5; 11]

// Pour une matrice carrée, on peut utiliser l'opérateur ^ pour calculer des
// puissances de matrice
[1 1; 0 1] ^2 // donne [1 1; 0 1] * [1 1; 0 1], c'est à dire [1 2; 0 1].

// On peut utiliser inv ou "^(-1)" pour calculer l'inverse:
inv([1 1; 0 1]) // donne [1 -1; 0 1]
[1 1; 0 1] ^ (-1) // idem


// COMPARAISONS ENTRE LES MATRICES (ATTENTION, PIEGE !)
// -------------------------------
//       _
//      / \
//     / | \
//    /  |  \
//   /   |   \
//  /         \
// /     o     \
///_____________\

// Attention, l'opération suivante (chantons tous en choeur "MERCI SCILAB !"):
m1 == m2
// ne renvoie pas "%t" ou "%f" ! Elle renvoie une matrice de "%t" et "%f" qui
// sont les résultats des comparaisons élément par élément. Pour m1, m2 comme
// précédemment, on aura donc une matrice constituée entièrement de "%f".

// Pour tester si deux matrices sont les mêmes, on utilise la fonction
// "isequal" (logique non ?). Par exemple :
isequal(m1, m2) // %f
isequal([1 2], [1 2]) // %t
isequal([1 2], [1 3]) // %f
isequal([1 1 1], [1 1]) // %f (tailles différentes)
[1 2] == [1 3] // [%t %f]
[1 2] == [1 2 3] // %f
[1 2 3] == [1] // [%t %f %f] (!)

// Bref : n'utilisez JAMAIS l'opérateur == entre deux matrices (à moins de très
// bien savoir ce que vous faites et pourquoi vous le faites).
