clear()
clf()
// Réglage de l'affichage
a = get("current_axes")
a.data_bounds = [-10.2 10.2 -10.2 10.2]
a.isoview = "on"

// Le but est que l'utilisateur puisse déplacer une flèche ou la faire pivoter
// (cet exercice est inspiré du célèbre langage "Logo" et de sa tortue, voir
// http://www.transum.org/software/Logo/ pour un exemple)

// Vous n'avez que la fonction bouger_tortue à compléter. Vous n'avez pas
// comprendre le fonctionnement interne des autres fonctions, juste comprendre
// ce qu'elles font grâce à leur documentation.

function fleche = creer_fleche_mobile(pos, vit)
	// Renvoie un vecteur de coordonnées "vit" dont l'origine est aux coordonnes
	// "pos". On peut ensuite déplacer cette flèche grâce à la fonction
	// "deplacer_fleche".
	pos2 = pos + vit
	xarrows([pos(1) pos2(1)], [pos(2) pos2(2)])
	fleche = gce()
endfunction

function deplacer_fleche(fleche, pos, vit)
	// Déplace la fleche passée en argument à une nouvelle position et l'oriente
	// selon le vecteur vit.
	pos2 = pos + vit
	fleche.data = [pos'; pos2']
endfunction

function [ok, action, param] = parse_commande(commande)
	// Analyse le paramètre "commande" (chaîne de caractères).   
	// Si la commande est invalide, "ok" est faux et il faut ignorer les autres
	// variables de retour.
	// Si la commande est valide les actions possibles sont:
	//		* 'a' pour avancer ; dans ce cas le paramètre donne le nombre de pas
	//		* 't' pour tourner ; dans ce cas le paramètre donne l'angle (en 
    // 	       degré, dans le sens trigo)
    //      * 'q' pour quitter ; dans ce cas, il faut ignorer le paramètre.
	ok = %f
	action = ''
	param = 0

	mots = strsplit(commande, " ")
	[n_mots, ignore] = size(mots)

	if n_mots == 1 & mots(1) == 'q' then
		action = 'q'
		ok = %t
		return
	end

	if n_mots <> 2 then
		return
	elseif mots(1) <> 'a' & mots(1) <> 't' then
		return
	end

	action = mots(1)
	[param reste] = strtod(mots(2))
	if reste <> '' | isnan(param) then
		return
	end

	if action == 'a' & param < 0 then
		return
	end

	ok = %t
endfunction

function [action, param] = demander_action()
    // Demande une action à l'utilisateur (demande plusieurs fois si nécessaire,
    // jusqu'à avoir une action correctement définie)
	ok = %f
	while ~ok
		rep = input("Entrer la commande: ", "string")
		[ok, action, param] = parse_commande(rep)
		if ~ok then
			disp("Commande invalide, ressayer.")
		end
	end
endfunction


function bouger_tortue()
    // Propose à l'utilisateur de déplacer la flèche. L'utilisateur tape des
    // commandes dans la console, la flèche se déplace sur la fenêtre de dessin.
	printf("Vous aller pouvoir déplacer la flèche grâce à deux commandes:\n")
	printf("* a <nombre positif>\n* t <nombre>\n* q\n\n")
	printf("Par exemple, la commande ''a 3'' fait Avancer la flèche sur 3 pas.\n")
	printf("La commande ''t 15'' fait Tourner la flèche de 15° (sens trigo).\n")
	printf("Pour finir, ''q'' Quitte le programme.\n")

	continuer = %t
	pos = [0; 0]
	vit = [1; 0]
	fleche = creer_fleche_mobile(pos, vit)
    // À compléter ici : 
    // * on demande	une commande à l'utilisateur.
    // * on fait bouger la tortue en conséquence (pour tourner, utiliser une
    //   matrice de rotation).
    // * on recommence jusqu'à ce que l'utilisateur rentre "q".
endfunction

bouger_tortue()

