// La fonction "grand" sert à produire des nombres aléatoires,
// l'argument "uin" indique qu'on veut produire des entiers,
// les deux derniers arguments (1 et 100 ici) indiquent entre les bornes
// entre lesquelles il faut tirer le nombre.
nb_a_trouver = grand(1,1, "uin", 1, 100)

disp("Le nombre à trouver est : " + string(nb_a_trouver))
