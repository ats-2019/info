clear()

// On s'intéresse à la résolution d'équations et d'inéquations du second
// degré.


//                    ***** INSTRUCTIONS *****
// Vous devez écrire le code de chaque fonction décrite ci-dessous.
// Une fois la fonction
// écrite, exécutez le programme ; vérifiez sur l'affichage que vos fonctions
// renvoient ce qu'il faut avant de passer à la fonction suivante !

function D=delta(a,b,c)
    // Cette fonction retourne le discriminant du polynôme du second degré
    //  aX² + bX +c (a, b et c sont des complexes).
endfunction

// Pour les fonctions nb_racines_R et nb_racines_C, on utilisera impérativement
// la fonction "delta".
function N=nb_racines_R(a,b,c)
    // Renvoie le nombre de racines réelles du polynôme aX² + bX + c (a, b et
    //  c sont des réels).
endfunction
    
function N=nb_racines_C(a,b,c)
    // Renvoie le nombre de racines complexes du polynôme aX² + bX + c (a, b et
    // c sont des complexes).
    
endfunction

// Certaines fonctions ne renvoient rien (comme la fonction "disp" par exemple);
// dans leur définition, il n'y a donc aucune valeur de retour.
// C'est le cas de la fonction affich_racines.
function affiche_racines(a,b,c)
    // Affiche les racines (réelles ou complexes) du polynôme aX² + bX + c 
    // (a, b et c sont des réels).
endfunction


// TESTS DES FONCTIONS :
disp("delta(1,2,3) est calculé à " + string(delta(1,2,3)) + " et devrait être -8")
disp("delta(-1,2,3) est calculé à " + string(delta(-1,2,3)) + " et devrait être 16")
disp("delta(-1,2,-3) est calculé à " + string(delta(-1,2,-3)) + " et devrait être -8")

disp("#########################")

disp("nb_racines_R(1,2,3) est calculé à " + string(nb_racines_R(1,2,3)) + " et devrait être 0")
disp("nb_racines_R(1,2,1) est calculé à " + string(nb_racines_R(1,2,1)) + " et devrait être 1")
disp("nb_racines_R(-1,2,3) est calculé à " + string(nb_racines_R(-1,2,3)) + " et devrait être 2")

disp("#########################")

disp("nb_racines_C(1,2,3) est calculé à " + string(nb_racines_C(1,2,3)) + " et devrait être 2")

disp("#########################")



