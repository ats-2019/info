//                    ***** INSTRUCTIONS *****
// Vous devez écrire le code de chaque fonction décrite ci-dessous.
// Une fois la fonction
// écrite, exécutez le programme ; vérifiez sur l'affichage que vos fonctions
// renvoient ce qu'il faut avant de passer à la fonction suivante !


function prix = prixTTC(prixHT)
    // Cette fonction prend en argument un prix hors taxe et renvoie le
    // prix toutes taxes comprises, c'est à dire en appliquant la TVA.
    // On prendra une TVA à 19.6%, c'est à dire qu'il faut multiplier
    // le prix hors taxe par 1.196 pour obtenir le prix toutes taxes.

endfunction



// NOUVELLE LOI! Le gourvernement a voté un nouveau taux de TVA à 4.2% (c'est
// une fiction hein, ne révez pas trop ;) ). Imaginons que nous ayons écrit un
// programme comptable calculant des prix TTC à 4200 endroits dans le code.
// Question: si nous avons écrit "à la main" la mulitplication par 1.196,
// combien de changements dans le code faut-il effectuer? Et si on utilise la
// fonction "prixTTC"?

// TESTS DES FONCTIONS :
disp("prixTTC(3) renvoie " + string(prixTTC(3)) + " et il doit renvoyer 3.588")
