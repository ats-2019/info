// Nous allons programmer un jeu du "pierre-feuille-ciseau".
// On définit ici des variables pour écrire un code plus lisible:

PIERRE = 0
FEUILLE = 1
CISEAU = 2

function resultat = joueur1_gagne(coup1, coup2)
    // Fonction renvoyant true si le joueur 1 gagne.
    // Cette fonction est déjà écrite, vous devez juste comprendre ce qu'elle
    // fait (le symbole & représente un "ET" et le symbole | représente le "OU")
    resultat = (
           ( coup1 == PIERRE & coup2 == CISEAU )
        | ( coup1 == FEUILLE & coup2 == PIERRE )
        | ( coup1 == CISEAU & coup2 == FEUILLE )
    )
endfunction

function coup = recupere_coup()
    // Cette fonction (à compléter) demande au joueur un coup valude (PIERRE,
    // FEUILLE ou CISEAU).
    // La fonction répète la question tant que la réponse n'est pas une de ces
    // trois valeurs.
endfunction


// Début effectif du jeu

disp("Bienvenue au jeu pierre-feuille-ciseau!")


rejouer = %t // %t représente la valeur "true"
while rejouer // Boucle principale
    disp("Joueur 1, à vous de jouer")
    coup1 = recupere_coup()

    disp("Joueur 2, à vous de jouer")
    coup2 = recupere_coup()

    if coup1 == coup2
        disp("Égalité !")
    else
        rejouer = %f // %f représente la valeur "false"
    end
end

// Conclusion du jeu
if joueur1_gagne(coup1, coup2)
    disp("Joueur 1 gagne !")
else
    disp("Joueur 2 gagne !")
end

// À faire une fois que le programme fonctionne: ajouter un argument "joueur"
// à la fonction "recupere_coup" pour que le début de la boucle principale soit:
// while rejouer // Boucle principale
//     coup1 = recupere_coup(1)
//     coup2 = recupere_coup(2)
