clear() // cette commande efface toutes les variables précédemment définies


// Les lignes précédées d'un double slash (//) sont des "commentaires" et ne sont
// interprétées par scilab. Ces commentaires sont utiles pour expliquer ce que
// le code est censé faire, il faut pas hésiter à les utiliser !

// Ici je me servirai de ces commentaires pour vous donner des consignes ou vous
// expliquer certains concepts.

//  ______ LES FONCTIONS ________
// En programmation informatique, les fonctions permettent de :
//   * découper le code de façon logique ;
//   * "encapsuler" les concepts : celui qui utilise la fonction n'a pas besoin
//     de savoir comment cette fonction est programmée ;
//   * éviter d'écrire plusieurs fois le même code.

// On peut distinguer deux phases :
//   * la première, appelée "définition" : on explique à scilab ce qu'il doit
//     exécuter lorsqu'on appelle la fonction ; en scilab, on utilise le mot clef
//     "function" pour définir une fonction.
//     Lorsqu'on veut retourner une valeur (le résultat d'un calcul par exemple)
//	   on indique le nom de la variable qu'on veut renvoyer.
//   * la deuxième, est l'appel effectif de la fonction.
//
// Exemple: voici la définition de la fonction appelée "aire_carre" ; cette
// fonction a un paramètre appelé "cote". Quand scilab lit ce code, il n'exécute
// rien, il mémorise les actions à réaliser pour la fonction "aire_carre" :
function aire=aire_carre(cote)
    aire = cote * cote // on aurait aussi pu écrire : aire = cote^2
endfunction

// Nous distinguons ici les variables d'entrées et les variables de sortie:
// * "cote" est une "variable d'entrée": c'est ce qu'on donne en paramètre à la
//   fonction
// * "aire" est une "variable de sortie": c'est le résultat du calcul effectué
//   par la fonction.

// Suite de l'exemple : ici on appelle la fonction, comme on le ferait en maths.
// Ici, on passe "5" en argument à la fonction. Avantage : on n'a pas à connaitre
// la formule permettant de calculer l'aire d'un carré et le code reflète
// ce qu'on est en train de faire ; ce n'est pas bien flagrant ici vu que le
// code de la fonction "aire_carre" est très simple, mais imaginez que cette
// fonction calcule la solution d'une équation différentielle non linéaire...
A = aire_carre(5)
disp("L''aire d''un carré de coté 5 est : " + string(A))

// On peut évidemment passer une variable en argument ; remarquez qu'ici la
// variable n'a pas le meme nom que le paramètre formel, de la meme façon qu'en
// maths on peut définir une fonction par "Soit f telle que f(x)=5x+3..." et
// calculer f(t).
c1 = 10
c2 = 20
aire_tot = aire_carre(c1) + aire_carre(c2)
disp("La somme des aires d''un carré de coté " + string(c1) + " et d''un carré de coté " + string(c2) + " est : " + string(aire_tot))


// ________PLUSIEURS ARGUMENTS ______
// Parfois un calcul nécessite plusieurs arguments. Par exemple, pour calculer
// l'aire d'un rectangle, nous avons besoin de connaître sa longueur et sa
// largeur. On sépare alors les arguments par une virgule:
function aire = aire_rectangle(longueur, largeur)
    aire = longueur * largeur
endfunction

A2 = aire_rectangle(15, 10)
disp('L'' aire d''un rectangle de longueur 15 et de largeur 10 est : ' + string(A2))


// _____PROCEDURES_______
// Parfois les fonctions sont juste une succession d'opérations ne donnant aucun
// résultat; de telles fonctions sont alors appelées "procédures". Par exemple
// une fonction d'affichage: la fonction suivante affiche un texte entre deux
// lignes de "*".

function afficher_important(text)
    disp("******************************")
    disp(text)
    disp("******************************")
endfunction


disp("Appel de ''afficher_important(""Salut !"")'':")
afficher_important("Salut !")

