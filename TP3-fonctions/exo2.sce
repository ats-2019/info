// Voici un script écrit sans fonctions ni procédures...
// Définissez et utilisez en quelques
// unes pour rendre le code plus lisible et compact !

// À l'exécution, le programme doit être rigoureusement le même du point de vue
// de l'utilisateur!


// Indications: on pourra adapter la fonction "afficher_important" du cours
// et créer une procédure "jeu" sans argument.
disp("************************************")
disp("Bienvenue dans mon super jeu!")
disp("************************************")


nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")


if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Jouons une deuxième fois!")


nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")

if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Jouons une troisième fois!")



nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")

if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Jouons une quatrième fois!")



nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")

if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Jouons une cinquième fois!")


nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")

if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Jouons une sixième fois!")


nombre1 = input("Entrez un premier nombre")
nombre2 = input("Entrez un second nombre")

if nombre1 ^ 2 +1 > nombre2
    disp("************************************")
    disp("Gagné !")
    disp("************************************")
then
    disp("************************************")
    disp("Perdu !")
    disp("************************************")
end
disp("Merci d''avoir joué à mon super jeu!")

