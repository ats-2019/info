clear()
clf()

// Les fonctions "creer_point_mobile" et "deplacer" sont faites pour vous
// faciliter la création d'animations. Ne cherchez pas à comprendre comment
// elles sont codées en première lecture, contentez vous de les utiliser.
function point = creer_point_mobile(marqueur, taille, fond)
	// Crée un point sur un graphique qu'on peut "déplacer" après sa création.
	// Par défaut, le point est placé à l'origine du repère (0, 0).
	plot(0, 0, marqueur, "marksize", taille, "markbackground", fond)
	point = gce()
endfunction

function deplacer(point, pos)
	// Déplace un point créé par la fonction creer_point_mobile aux coordonnes
	// "pos".
	point.children.data = pos
endfunction

// Réglage de l'affichage
a = get("current_axes")
a.data_bounds = [-0.2 1.2 -0.2 1.2]
a.isoview = "on"


// Cette variable indique le temps (en ms) entre chaque image. Plus cette
// variable sera grande, plus lente sera l'animation.
TEMPS_REPOS = 10

// Le but de ce script est de simuler un billard (sans frottement).
// Lisez le script, essayez de comprendre ce qu'il fait, puis exécutez le.
// Ensuite, modifiez le (suivre les indications).

// "NB_PAS" est le nombre de pas qui vont être réalisés.
NB_PAS = 1000
// "pos" stocke la postition courante de la bille
pos = [0 0]
// "vit" stocke la vitesse courante de la bille
vit = [3 1]
// "dt" est l'intervalle de temps entre chaque pas
dt = 0.001


// On crée la boule de billard : ce sera un cercle ("o") bleu ("b") de taille 5
// et dont le fond est bleu ("b").
boule = creer_point_mobile("ob", 5, "b")

// À faire: dessiner le bord du billard grâce à la fonction plot (on pourra
// prendre un billard carré dont les sommets sont (0,1), (0,0), (1,0) , (1,1))
//...
for i=1:NB_PAS
	// À chaque pas, on fait avancer la bille selon sa vitesse
	pos = pos + dt * vit

	// À faire : gérer le rebond contre les parois du billard (la vitesse est
	// modifiée)

	//...

	deplacer(boule, pos)
	sleep(TEMPS_REPOS)
end

// BONUS: 
// * gérer deux boules en même temps sur le billard. Faites en sorte
// qu'il n'y ait pas de "copié collé" pour gérer le changement de vitesse des
// boules (indication: créer une fonction prenant en paramètre la position et la
// vitesse d'une boule).
//
// * gérer n boules en même temps. On pourra utiliser des "list"s (google is ur
// friend).
