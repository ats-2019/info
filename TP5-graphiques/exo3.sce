// Consigne : dessiner un cercle de centre (0,0) et de rayon 1.

// Indication : un tel cercle est l'ensemble {(cos(t), sin(t)); t € [0, 2pi]}.
