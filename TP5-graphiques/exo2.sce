clf()
clear()
// On peut appliquer les fonctions mathématiques usuelles aux tableaux :
// par exemple cos([0,%pi/2, %pi]) donne [1, 0, -1]. C'est très utiles pour
// représenter des fonctions.

// Par exemple:
xs = linspace(0, 2*%pi, 100) // donne un tableau de taille 100 de nombres 
                             // régulièrement espacés entre 0 et 2pi.
plot(xs, cos(xs))

// Consigne : visualiser que pour x réel, exp(x) >= 1+x et pour x négatif,
// exp(x) <= 1 + x +x^2/2.

