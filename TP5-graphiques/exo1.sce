clf() // "clear figure": efface les dessins déjà réalisés
clear()

// La fonction plot sert à afficher des graphiques. Elle prend deux arguments :
// le premier est un tableau indiquant les abscisses et le second est un tableau
// indiquant les ordonnées. Scilab relie ensuite les points ainsi définis.

// Exemple :

// la commande suivante va tracer les points de coordonnées (0,3), (1,2) puis 
// (5,6) en les reliant.
plot([0 1 5], [3 2 6]) 

// Consigne 1 : en utilisant la fonction plot, tracer le carré dont coordonnées 
// des points sont (0,0), (0,1), (1,1), (1,0).

// Consigne 2 : remplacer "plot" par "comet". Quelle est la différence entre 
// les deux ?

