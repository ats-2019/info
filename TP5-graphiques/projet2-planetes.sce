clear()
clf()

function point = creer_point_mobile(marqueur, taille, fond)
	// Crée un point sur un graphique qu'on peut "déplacer" après sa création.
	// Par défaut, le point est placé à l'origine du repère (0, 0).
	plot(0, 0, marqueur, "marksize", taille, "markbackground", fond)
	point = gce()
endfunction

function deplacer(point, pos)
	// Déplace un point créé par la fonction creer_point_mobile aux coordonnes
	// "pos".
	point.children.data = pos
endfunction

// Réglage de l'affichage
a = get("current_axes")
a.isoview = "on"
a.data_bounds = [-3.5e11 3.5e11 -3.5e11 3.5e11]

// Cette variable indique le temps (en ms) entre chaque image. Plus cette
// variable sera grande, plus lente sera l'animation.
TEMPS_REPOS = 10

// Le but de ce script est de simuler le système solaire (et dans un premier
// temps, la rotation de la terre autour du soleil). La structure initiale est
// la même que le projet précédent avec le billard.

// Pour commencer le soleil est au centre (0,0) du système : il sera représenté
// par un cercle jaune ("oy") de taille "40" et de fond jaune ("y").
soleil = creer_point_mobile("oy", 40, "y")
terre = creer_point_mobile("ob", 10, "b")

// Les masses sont en kg
M_TERRE = 5.972e24
M_SOLEIL = 1.989e30

// La constante de gravitation universelle (en S.I.)
G = 6.67408e-11

// "NB_PAS" est le nombre de pas qui vont être réalisés.
NB_PAS = 1000
// "pos" stocke la postition courante de la terre (coordonnées en mètres)
pos = [149e9 0] // le "e" se lit "10 puissance"
// "vit" stocke la vitesse courante de la terre (coordonnées en m/s)
vit = [0 29790]
// "dt" est l'intervalle de temps entre chaque pas (en secondes)
dt = 3600*24

for i=1:NB_PAS
	// À chaque pas, on fait avancer la terre selon sa vitesse
	pos = pos + dt * vit
	// À faire : gérer la modification de la vitesse de la terre qui est attirée
    // par le soleil (indication: la variation de la vitesse est l'accélaration,
    // "somme des forces = ma", et ici il n'y a qu'une seule force)

    // ...
    
	deplacer(terre, pos)
	sleep(TEMPS_REPOS)
end

// À faire ensuite :
// 1) modifier les valeurs initiales de la vitesse et/ou de la position afin 
//    d'obtenir une trajectoire (vraiment) elliptique, hyperbolique (la terre 
//    sort de l'attraction terrestre), ou crashique (la terre entre collision
//    avec le soleil).

// 2) Pour l'instant, on néglige l'effet de la terre sur le soleil. Modifier le 
//    programme (sauvegarder une version du programme "qui marche" avant !)
//    pour prendre également en compte cette interaction. Le soleil n'est plus 
//    alors fixe au point (0,0). On pourra ensuite observer que le lorsque les
//    masses des astres sont proches, les deux corps tournent tous les deux
//    autour du centre de gravité du système.

// Bonus (à faire dans l'ordre que vous voulez): 
// * rajouter la gestion de la collision entre les astres (par exemple on 
//   arrête la boucle et on affiche "FIN DU MONDE !" quand la terre est trop 
//   proche du soleil).

// * gérer tout le système solaire. Il faudra pour cela probablement repenser
//   tout le programme : on veut gérer N corps (avec N un entier quelconque).
//   On pourra utiliser une matrice poss de taille Nx2 (une ligne) pour chaque
//   astre ainsi qu'une matrice vits de même taille. À chaque pas, pour chaque
//   astre il faudra calculer l'influence de tous les autres corps.


