clear()

// On considère le tableau suivant
t = [57, 55, 89, 50, 52, 61, 2, 48, 45, 44, 56, 85, 92, 39, 63, 89, 0, 75, 84, 71, 22, 57, 59, 10, 19, 74, 80, 98, 69, 23, 71, 76, 67, 75, 77, 49, 96, 49, 25, 55, 33, 40, 44, 60, 89, 5, 64, 45, 24, 27, 51, 98, 70, 22, 40, 45, 100, 1, 68, 27, 24, 8, 89, 93, 58, 4, 27, 13, 9, 1, 77, 52, 13, 56, 31, 40, 62, 11, 97, 57, 18, 78, 81, 9, 56, 79, 4, 94, 37, 83, 84, 41, 24, 87, 45, 47, 42, 39, 45, 38]

// Compléter les commandes suivantes en remplaçant les "// ..." par des 
// expressions (et uniquement ceux-là !):
taille = 0 // ... 
disp("Le tableau t comporte " + string(taille) + " éléments.")

element12 = 0 // ...
disp("Le douxième élément du tableau t est " + string(element12))

somme = 0
for i=1:taille
	// ...
end
disp("La somme de tous les éléments du tableau est " + string(somme))


// Écrire un script qui affiche "43 est dedans !" si 43 est dans t ou 
// "43 n'est pas dans t !"


