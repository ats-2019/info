clear()

// Créer un tableau A  contenant 1000 éléments valant tous 5.
// Indication : utiliser "ones" et la multiplication d'un vecteur par un nombre


// ...

// Créer un tableau B contenant tous entiers pairs entre 0 et 1000.
// Indication : utiliser la syntaxe "a:b:c".

// ...

// Créer un tableau C contenant 1000 éléments : les 500 premiers étant 2,
// les 500 derniers étant 5.
// Indication : initialiser C grâce à la fonction "zeros" puis
// utiliser une (ou deux) boucle(s) for.