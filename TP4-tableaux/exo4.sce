clear()
// écrire deux fonction, une fonction appellée "maximum",
// une fonction appelée "minimum", et une fonction "somme" prenant un tableau en
// entrée et renvoyant respectivement le maximum, le minimum et la somme de tous
// les éléments du tableau.




// TESTS (décommentez lorsque vous voulez tester)
// failed = %f
// if maximum([1 2 3]) <> 3 then
// 	failed = %t
// 	disp("Le maximum de [1 2 3] est 3 et la fonction renvoie " + string(maximum([1 2 3])))
// end

// if maximum([3 2 1]) <> 3 then 
// 	failed = %t
// 	disp("Le maximum de [3 2 1] est 3 et la fonction renvoie " + string(maximum([1 2 3])))
// end

// if maximum([1 3 2]) <> 3 then
// 	failed = %t
// 	disp("Le maximum de [3 2 1] est 3 et la fonction renvoie " + string(maximum([1 2 3])))
// end

// if minimum([1 2 3]) <> 1 then
// 	failed = %t
// 	disp("Le minimum de [1 2 3] est 1 et la fonction renvoie " + string(minimum([1 2 3])))
// end

// if minimum([1 3 2]) <> 1 then
// 	failed = %t
// 	disp("Le minimum de [1 3 2] est 1 et la fonction renvoie " + string(minimum([1 2 3])))
// end


// if minimum([3 2 1]) <> 1 then
// 	failed = %t
// 	disp("Le minimum de [3 2 1] est 1 et la fonction renvoie " + string(minimum([1 2 3])))
// end

// if ~failed then
// 	disp("Tous les tests passés avec succès")
// end





