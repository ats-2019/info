clear()
// Le projet ici est de créer un mastermind. Le jeu est (presque) entièrement
// programmé en bas du fichier, il reste à remplir les fonctions dont les
// commentaires donnent le rôle précis.

// Voici ce que le programme fera : l'ordinateur choisit aléatoirement
// 4 couleurs (représentées par les chiffres 1, 2, 3, ..., 9) dans
// un certain ordre. Par exemple "4 2 4 3" (on peut faire des répétitions).

// Le joueur devra ensuite proposer une combinaison de
// couleurs. Par exemple, il propose "3 4 3 3", le programme lui indique
// alors le nombre de couleurs "bien placées" et de "mal placées".
// Dans l'exemple, il y a une couleur mal placée (le 4) et une couleur
// bien placée (le 3). Le programme affiche donc "1 mal placée, 1 bien placée"
// (sans préciser la quelle, c'est ça qui est rigolo !).

// Le joueur propose successivement plusieurs combinaisons jusqu'à ce qu'il
// trouve la même combinaison que l'ordinateur.


// Maintenant, au travail ! Les "// ..." indiquent là il faut compléter du code.
// N'hésitez pas à tester les fonctions individuellement dans des fichiers
// séparés.



function combi=genere_combi()
	// Renvoie une combinaison générée aléatoirement de 4 chiffres.
	combi=zeros(1, 4)
	// ...
	// Indication : on rappelle qu'on peut générer un nombre entier
	// aléatoire entre a et b par <<grand(1,1, "uin", a, b)>>. On peut
	// aussi utiliser une boucle.
endfunction

function combi=demande_combi()
	// Demande à l'utilisateur de rentrer une combinaison.
	// (cette fonction est déjà réalisée, rien à faire sauf en "bonus").
	printf("Rentre une combinaison (4 chiffres séparés par des espaces) :")
	combi = scanf("%d %d %d %d")
endfunction

function nb=nb_elements_identiques(tab1, tab2)
	// Prend en argument deux tableaux tab1 et tab2 de même taille et retourne
	// le nombre d'éléments qui sont exactement aux même places dans
	// les deux tableaux. Exemples :
	// nb_elements_identiques([1, 2, 3], [4, 5, 6]) == 0
	// nb_elements_identiques([2, 3, 5], [2, 5, 3]) == 1 (le 2)
	// nb_elements_identiques([3, 3, 5], [1, 5, 3]) == 0
	// nb_elements_identiques([1, 3, 5], [1, 3, 5]) == 3 
	nb = 0
	// ...
endfunction


function nb=nb_elements_communs(tab1, tab2)
	// Prend en argument deux tableaux tab1 et tab2 de même taille et 
	// contiennent uniquement des chiffres de 1 à 9.
	// Retourne le nombre d'élements qui sont dans les deux tableaux peu importe
	// leur position. Exemples :
	// nb_elements_communs([1,3,5], [1,5,4]) == 2 (le 1 et le 5)
	// nb_elements_communs([4,3,5], [1,5,4]) == 2 (le 5 et le 4)
	// nb_elements_communs([4,3,5,4], [1,5,4,4]) == 3 (le 5 et les deux 4)
	// nb_elements_communs([4,3,5,4], [1,4,4,7]) == 2 (les deux 4)
	// nb_elements_communs([4,3,5,4], [1,8,4,7]) == 1 (un 4)
	// nb_elements_communs([1,2,3], [4,5,6]) == 0
	compteurs = zeros(1, 9)
	nb = 0
	// ...
	// Indication : utiliser le tableau "compteurs" pour compter le nombre
	// de 1, 2, 3..., 9 dans tab1. Puis dans un second temps parcourir tab2
	// et regarder si l'élément en cours a un compteur strictement positif.
	// Si c'est le cas, réduire son compteur de 1 et augmenter la variable
	// "nb" de 1.
endfunction


function partie()
	// Joue une partie (pas grand chose à faire mais un petit peu quand même !). 

	combi_secrete = genere_combi()
	// Pour le débuggage, à supprimer après bien sûr !!!
	disp("Combinaison secrète : ")
	disp(combi_secrete)

	trouve = %f
	nb_essais=0
	while ~trouve // "~" est la négation logique
		combi_joueur = demande_combi()
		bien_places = nb_elements_identiques(combi_joueur, combi_secrete)
		if bien_places == 4 then
			trouve = %t
		else
			mal_places = 0 //...
			printf("%d mal placés et %d bien placés.\n", mal_places, bien_places)
		end
		// ...
	end

	printf("Vous avez trouvé en %d essais.\n", nb_essais)
endfunction

partie()


// Bonus (à faire dans l'ordre que vous voulez ; si votre programme
// fonctionne, faites en un copie dans un autre fichier avant de le modifier !):

// * Améliorer l'affichage : ajouter des cadres (grâce à des lignes de ### ou 
// ___ ou **** ou ...), un texte de présentation, ...
// On pourra se documenter sur la fonction "printf" ("google is ur friend").

// * Pour l'instant, on ne peut faire qu'une seule partie. Modifier le programme
// pour qu'à la fin de la partie, on nous propose de faire une autre partie
// (on gardera la fonction "partie" inchangée).

// * Si vous avez fait le point précédent, proposez des statistiques au joueur 
// sur les différentes parties ( Par exemple: 
// "Tu as joué 5 parties, en moyenne tu as trouvé en 8.2 essais").
// On pourra pour cela modifier la fonction "partie" pour lui faire renvoyer le
// nombre d'essais effectués par le joueur pendant la partie.

// * Proposer au joueur de modifier la difficulté en changeant la longueur des
// combinaisons (par exemple, le joueur peut choisir entre des combinaisons de
// 4, 5 ou 6 chiffres).
// On pourra pour cela rajouter l'argument "long_combi" aux fonctions "partie",
// "genere_combi", "demande_combi".

// * La fonction "demande_combi" n'est pas très "user friendly". Elle n'indique
// pas au joueur que la combinaison rentrée est invalide (et cela compte comme
// un essai !). Modifier cette fonction pour redemander au joueur la combinaison
// tant qu'elle n'est pas correcte.
// On pourra se documenter sur la fonction "scanf" (et/ou faire des tests en 
// console).