clear() // cette commande efface toutes les variables précédemment définies


//               ****** LES TABLEAUX ******

// Vous pouvez copier/coller dans la console les différents exemples pour voir
// ce que scilab renvoie.

// 1. Manipulations de base
// ------------------------

// Mettons qu'on veuille stocker toutes les notes d'un élève pour ensuite
// procéder à des calculs. On pourrait procéder ainsi :
note1 = 15
note2 = 17
note3 = 19 // Bravo !
moyenne = (note1 + note2 + note3)/3

// Ça marche, mais ce n'est pas très pratique : si on veut rajouter une note,
// on doit créer une nouvelle variable, puis modifier le calcul de la moyenne.

// On stocke alors toutes les notes dans un "tableau". Le début 
// d'une tableau est marquée par [, la fin par ] et les éléments sont séparés par 
// des virgules. Par exemple, l'exemple précédent se ré-écrit :
notes = [15, 17, 19]

// On trouve également la syntaxe suivante, sans virgule :
notes = [15 17 19]



// On accède aux éléments grâce à la syntaxe : notes(index). Les tableaux sont 
// indexées à partir de 1
notes(1) // --> 15
notes(2) // --> 17
notes(3) // --> 19

// On peut modifier les notes :
notes(2) = 5 // maintenant notes contient le tableau : [15, 5, 19]

// On peut alors calculer la moyenne ainsi :
somme = 0
for i=1:3 // i va prendre les valeurs 1, 2 puis 3
    somme = somme + notes(i)
end
moyenne = somme /3

// C'est mieux, mais si on veut rajouter une note on doit encore modifier le 
// code précédent en transformant toutes les apparitions de 3 en 4.
// En regardant bien, ce 3 n'est rien
// d'autre que le nombre d'éléments dans le tableau "notes". La commande pour 
// connaître le nombre d'éléments d'un tableau est length.
// Exemple :
n = length(notes) // n est le nombre d'éléments dans la tableau notes.
somme = 0
for i=1:n // i va prendre les valeurs de 1,2,3 ... jusqu'à n
    somme = somme + notes(i)
end
moyenne = somme / n

// Ainsi le bout de code précédent permet de calculer la moyenne quelque soit
// le nombre de notes dans le tableau.


// 2. Les joies de scilab
// ----------------------

// La partie précédente est valable dans quasiment tous les autres langages de
// programmation (excepté que le premier élément est en général indexé par
// 0 au lieu de 1). Voici quelques syntaxes  et fonctions spécifiques à scilab:


// * On peut construire un tableau de taille arbitraire grâce aux
// commandes "ones" et "zeros" construisant des tableaux remplis respectivement
// de 1 et de 0. Par exemple:
ones(1, 3) //        ---> construit [1, 1, 1]
ones(1, 5) //        ---> construit [1, 1, 1, 1, 1]

zeros(1, 3) //       ---> construit [0, 0, 0]
zeros(1, 5) //       ---> construit [0, 0, 0, 0, 0]

// Remarque : scilab manipule en fait des matrices et le premier argument donne
// le nombre de lignes. Pour l'instant nous ne manipulerons que des tableaux à
// une seule ligne, donc le premier argument sera toujours 1.


// * Pour créer des séquences qui se suivent, on peut utiliser la syntaxe
// "debut:fin" ou bien "debut:pas:fin". Exemples :
1:5       //         ---> [1, 2, 3, 4, 5]
4:6       //         ---> [4, 5, 6]
3:2:10    //         ---> [3, 5, 7, 9] (de 3 à 10 par pas de 2)
10:-1:1   //         ---> [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]



// * Les tableaux sont en fait des vecteurs, sur les quels ont peut faire
// des opérations. Par exemple:
[1,2] + [4,8] //    ---> donne le tableau [5, 10]
5* [2,3,7] //       ---> donne le tableau [10, 15, 35]
[2, 3] .* [4, 7] // ---> donne le tableau [8, 21] 
[1,2,3] .^ 3  //    ---> donne le tableau [1, 8, 27]
// ATTENTION : noter le point devant l'étoile !

// ATTENTION également aux tailles de tableaux:
[1, 2] + [1, 2, 3] // ---> ERREUR
