age = input("Quel âge as-tu ?")
if age < 18 then
	disp("Désolé petit, ici c''est pour les grands !")
else
	disp("Que fais-tu encore dehors ? Rentre vite t''amuser !")
end

disp("Au revoir")
		
// ceci est un commentaire
// les opérateurs de comparaison :
// égalité :  ==
// différent : <>
// supérieur : >
// supérieur ou égal : >=
// inférieur : <
// inférieur ou égal : <=
